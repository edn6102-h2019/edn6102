# Production du LaTeX

LaTeX est un logiciel qui permet de produire des textes mis en page de haute qualité à des fins d'impression.

Pour une introduction détaillée, cf. [ce livre](https://geekographie.maieul.net/95)

Le principe de LaTeX est d'avoir une texte édité en plein texte (_plain text_) et balisé, qui est ensuite processé par un programme (LaTeX) pour produire une sortie mise en page - en dvi ou Pdf.

Pour produire un document LaTeX vous avez donc besoin de

1. Une distribution de LaTeX - vous pouvez la télécharger [ici](https://www.latex-project.org/get/)
2. Un éditeur de texte dans lequel éditer le document (qui a l'extension .tex). Par exemple atom. L'éditeur de texte peut avoir la fonctionalité de surlignage de la syntaxe LaTeX
3. Un terminal pour exécuter le programme LaTeX
4. Un lecteur de Pdf pour visualiser le document.

La distribution LaTeX intègre un processeur qui produit une sortie Pdf à partir du fichier source `.tex`. Il en existe plusieurs, XeLaTeX, LuaTex, PdfLaTeX.

## La structure du document

Un document tex est structuré en deux parties:

1. Le préambule
2. Le corps du texte

### Le préambule

Le préambule est la partie du document tex qui définit la mise en forme. On y déclare l'ensemble des règles graphiques qui seront ensuite appliquées au document.

Syntaxe de base d'une balise LaTeX

      \fonction[options]{valeur}

Le premier élément, nécessaire dans tout document LaTeX est la déclaration du type de document.

Par exemple:

```tex
\documentclass[10pt,french]{book}
```

Cette balise déclare que le document en question est un livre, que la taille de police par défault est 10 points et que la langue principale du document est le français.

LaTeX intègre les règles de composition du texte appliquées traditionnellement dans l'imprimerie

On déclare ensuite dans le préambule les paquets qui seront utilisés. On "appelle" un paquet avec la commande (ou fonction, ou macro) `\usepackage[options]{paquet}`. Les paquets sont généralement un ensemble de macros permettant d'ajouter des fonctionnalités de mise en forme ou de gestion du contenu.

Par exemple

```latex
\usepackage[french, english]{babel}
\usepackage[paperheight=17.463cm,paperwidth=10.795cm,top=2.2cm,bottom=1.8cm,inner=1.5cm,outer=1.5cm]{geometry}
```

Le premier paquet ici - babel - permet d'avoir une gestion plus poussée des langues. Par exemple, dans ce cas, nous avons déclaré l'usage de deux langues et, dans le document, nous pourrons passer de l'une à l'autre avec une commande spécifique (`\selectlanguage`).

Le second paquet - geometry - permet de spécifier un particulier layout de page (taille, marges etc.).

Il y a une énorme quantité de paquet, et selon les usages il est toujours possible de trouver un paquet pour résoudre un problème particulier.
Le principe est de chercher le paquet correspondant au besoin que l'on exprime. Par exemple: gérer du multicolonne, gérer les exergues, des images, des langues particulières, etc.

Dans le préambule on peut finalement déclarer les métadonnées principales du document comme le titre, l'auteur et la date:

```
\title{On met le titre ici}
\author{Nom de l'auteur}
\date{Date ici}
```

### Le corps du texte


Après le préambule, commence le corps du document, à savoir la partie du document qui contient les informations qui seront ensuite affichées dans le Pdf.

Le corps du texte commence avec la commande:

```
\begin{document}
```

Et doit impérativement finir avec la commande

```
\end{document}
```

qui doit être la dernière ligne dans le fichier .tex

`\begin{}` et `\end{}` définissent un _environnement_. Un environnement est une partie de texte auquel on souhaite appliquer une fonction ou une mise en forme particulière.  La syntaxe est la suivante `\begin{nom de l'environnement}` + `\end{nom de l'environnement}`.

Par exemple pour avoir un texte centré :

      \begin{center}
      Mon texte centré.
      Une nouvelle ligne.

      Un nouveau paragraphe.
      \end{center}


Au tout début du corps du texte on peut organiser les pages de titre.

La commande

```
\maketitle
```

affiche le titre tel qu'il est déclaré dans les métadonnées

La commande

```
\tableofcontents
```

affiche une table des matières.

Ensuite nous pouvons lister les principales commandes qu'on peut trouver dans le corps du texte.

D'abord les commandes qui permettent de déclarer les titres:

```
\part{Titre de la partie}
\chapter{Titre du chapitre}
\section{Titre de la section}
\subsection{Titre de la sous-section}
\subsubsection{Titre de la sous-sous-section}
\subsubsubsection{Titre de la sous-sous-sous-section}
```

Remarque : les balises ```\part``` et ```\chapter``` ne sont disponibles que dans les classes de document book et report.

- Les notes à bas de page s'expriment ainsi:

      \footnote{Ma note}

- L'italique:

      \textit{Texte en italique}

- Le gras:

      \textbf{Texte en gras}


- Les listes numérotées:

      \begin{enumerate}
      \item Mon premier item
      \item Mon deuzième item
      \item Mon troisième item
      \end{enumerate}

## Exécuter LaTeX

Pour exécuter LaTeX, lancer dans le terminal:

      xelatex monfichier.tex


Il est nécessaire lancer la commande 2 fois si votre document comprend des labels (par exemple des titres des sections). Cela permet à LaTeX de les indexer.

Cette commande produira plusieurs fichiers dont un fichier `monfichier.pdf` affichable avec un lecteur de Pdf.

## LaTeX dans la chaîne Pandoc

Nous avons vu que Pandoc est capable de produire une sortie Pdf par défaut.

### Workflow #1 - utiliser le template LaTeX par défaut

Lancez la commande (sans oublier d'ajouter l'option `--bibliography` si nécessaire)

      pandoc --standalone -f markdown -t latex monfichier.md monfichier.yaml -o monfichier.tex
      xelatex monfichier.tex
      xelatex monfichier.tex

Puis visualisez le fichier `monfichier.pdf`.

Nous allons améliorer cette sortie par défaut en utilisant [les variables Pandoc disponibles pour LaTeX](http://pandoc.org/MANUAL.html#variables-for-latex).

Vous pouvez ajouter dans les métadonnées de votre document les différentes variables (et valeurs) YAML. Par exemple :

```yaml
papersize: letter
fontsize: 10pt
documentclass: book
colorlink: purple
bibliography:  monfichier.bib
```

Vous pouvez aussi ajouter dans la commande Pandoc différentes options (voir [les options Pandoc](http://pandoc.org/MANUAL.html#options-affecting-specific-writers)). Par exemple :

- `--toc` ajoutera une page de Table des Matière (Table of content si le langage principal du document est `en`).
- `--top-level-division`, en suivant le manuel Pandoc à ce sujet :
  > The hierarchy order is part, chapter, then section; all headers are shifted such that the top-level header becomes the specified type. The default behavior is to determine the best division type via heuristics: unless other conditions apply, `section` is chosen. When the LaTeX document class is set to `report`, `book`, or `memoir` (unless the `article` option is specified), `chapter` is implied as the setting for this option. [...]

Après (ou à la place de) ce travail de personnalisation, il est possible de travailler encore la mise en page du livre en éditant directement le fichier intermédiaire `.tex`.

Sans toucher au corps du texte (on considère que Pandoc produit un corps du texte correct), il est effectivement possible d'apporter des modifications dans le préambule du fichier `.tex`.  

Nous vous proposons dans cet exercice de remplacer le préambule du fichier `.tex` produit par Pandoc avec le préambule du [gabarit](/ressources/gabarit.tex) fourni dans le dossier `ressources`.

Une fois le préambule remplacer, il suffit de relancer la commande xelatex :

      xelatex monfichier.tex
      xelatex monfichier.tex

et de visualiser la nouvelle sortie `monfichier.pdf`.

#### Debug

Il est souvent nécessaire de _débugger_ son fichier `.tex`, Pandoc ne pouvant gérer tous les cas particuliers.

Quelques cas particuliers à traiter :
  - caractères spéciaux
  - gestion des veuves&orphelines
  - gestion du overfull hbox
    - syllabation
  - images
  - ...


### Workflow #2 - personnaliser le template LaTeX

Il est aussi possible de créer son propre template LaTeX et de l'utiliser à la place du template par défaut. Cela permettra notamment de choisir en amont le gabarit de l'ouvrage que l'on cherche à produire.

Pour visualiser le template Latex par défaut :

      pandoc -D latex > template.latex

Nous pouvons maintenant modifier ce template pour générer notre ouvrage personnalisé.

Copiez le template par défaut, et ouvrez le dans Atom.

      cp template.latex montemplate.latex
      atom montemplate.latex

Avant de modifier le template, nous allons étudier un [gabarit](/ressources/gabarit.tex) déjà créé.

Attention: le gabarit n'est pas un template. Il ne contient pas de variables Pandoc.

En utilisant le gabarit comme référence, modifiez le fichier montemplate.latex de manière à reproduire le gabarit donné.

Vous pouvez alors lancer les commandes.

      pandoc --standalone  --template=montemplate.latex -f markdown -t latex monfichier.md monfichier.yaml -o monfichier.tex
      xelatex monfichier.tex
      xelatex monfichier.tex

et visualiser la nouvelle sortie Pdf.

---
voir la suite [[parcours/08_livreEpub](parcours/08_livreEpub.md)]
