**Sommaire**
<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Quelques outils indispensables](#quelques-outils-indispensables)
	- [un éditeur de texte/code](#un-diteur-de-textecode)
	- [un convertisseur de format](#un-convertisseur-de-format)
	- [un compte sur framagit.org](#un-compte-sur-framagitorg)
	- [un gestionnaire de version : Git](#un-gestionnaire-de-version-git)
	- [LaTeX](#latex)
- [Prise en main de Git](#prise-en-main-de-git)
	- [Exercice - créer son premier répertoire](#exercice-crer-son-premier-rpertoire)
- [Prise en main de Pandoc](#prise-en-main-de-pandoc)
	- [Exercice de conversion](#exercice-de-conversion)
	- [Les templates dans Pandoc](#les-templates-dans-pandoc)

<!-- /TOC -->

# Quelques outils indispensables

Pour la suite de l'atelier vous aurez besoin
d'installer les outils ci-dessous. L'utilisation de votre ordinateur personnel est recommandée.

## un éditeur de texte/code

Les éditeurs de code sont plus ou moins simples selon les fonctionnalités qu'ils offrent. À minima pour l'atelier, l'éditeur devra pouvoir colorer la syntaxe Markdown, LaTeX, html et css (ce qu'ils font tous).

Suggestion : [Atom.io](https://atom.io/) qui fonctionnera sur toutes les plateformes.

Voir d'autres éditeurs sous [Windows](https://alternativeto.net/software/atom/?platform=windows), [Max](https://alternativeto.net/software/atom/?platform=mac), [Linux](https://alternativeto.net/software/atom/?platform=linux).

## un convertisseur de format

Un convertisseur de format permet comme son nom l'indique de convertir le format d'un document en un autre format. Le convertisseur génère alors un nouveau document.

Nous utilisons [Pandoc](https://pandoc.org) en ligne de commande.

- Installation de pandoc : https://pandoc.org/installing.html
- Documentation à consulter en cas de problème, pour les cas particuliers : https://pandoc.org/MANUAL.html

Nous aurons aussi besoin de l'extension **pandoc-citeproc** pour gérer les références bibliographiques.

## un compte sur framagit.org

Sur [framagit.org](https://framagit.org/users/sign_in?redirect_to_referer=yes), créez vous un compte. Cela vous sera utile pour :
1. partager votre code avec moi (et la classe si vous le souhaitez),
2. publier des documents en ligne,
3. [participer au wiki](https://framagit.org/edn6102-h2019/edn6102/wikis/home) de ce répertoire.

Cliquez sur le bouton [Request Access] pour demander un accès au répertoire.

**Merci de m'envoyer ensuite votre username.**

## un gestionnaire de version : Git

Le gestionnaire de version Git va vous permettre de communiquer votre code à framagit. Nous verrons ensemble comment l'utiliser.

- Linux: déjà installé
- Mac: déjà installé sinon, dans un terminal :
  - installer homebrew : `/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`
  - installer Git avec homebrew : `brew install git`
- Windows : [installez Git pour Windows](https://git-scm.com/downloads)

## LaTeX

LaTeX est un langage et un système de composition de documents.

Vous pouvez télécharger Latex [depuis le site officiel](https://www.latex-project.org/get/)

---
voir la suite [[parcours/04_edition](parcours/04_edition.md)]
