# Introduction générale

**Objectifs de l'atelier:**

- créer et éditer des documents divers
- devenir autonome dans l'utilisation de chaines de publications alternatives
- comprendre les enjeux liés aux formats et aux outils
- savoir trouver l'information qu'il vous manque

**Fonctionnement de l'atelier**

- ressource principale : https://framagit.org/laconis/edn6102
- un wiki pour collaborer : https://framagit.org/laconis/edn6102/wikis/home

**Evaluation**

Aucun rendu n'est demandé pour cet atelier, qui sera évalué entièrement sur la présence et la participation.

---
Voir la suite [[01_theorie](./01_theorie.md)]
