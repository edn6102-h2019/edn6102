# EDN6102 Outils de rédaction numérique pour la publication

Bienvenu dans l'atelier EDN6102. Vous trouverez ici toutes les ressources pour suivre l'atelier et faire les exercices.

Avant de venir à l'atelier, merci [de vous créer un compte sur Framagit](https://framagit.org/users/sign_in?redirect_to_referer=yes) (et de m'envoyer votre username) et [d'installer les outils nécessaires](./parcours/03_outils.md).

## Plan de cours

### Samedi 12 janvier

0. Introduction générale ([parcours/00_introduction](./parcours/00_introduction.md))
1. Un peu de théorie ([parcours/01_theorie](./parcours/01_theorie.md))
    - Gestion de fichiers : arborescence, nomenclature et versionnage
    - Formats de texte
    - Chaines éditoriales
    - Edition savante
2. Exercice 1 - Stylo ([parcours/02_stylo](./parcours/02_stylo.md))
    - présentation
    - édition d'un article
    - export d'un article

_pause déjeuner_

3. Outils ([parcours/03_outils](./parcours/03_outils.md))
    - Installation des outils
    - Prise en main de git ([parcours/03b_git](./parcours/03b_git.md))

_pause_

4. Exercice 2 - édition ([parcours/04_edition](./parcours/04_edition.md))
    - Prise en main de Pandoc
    - Exercice de conversion
    - Templating


## Samedi 19 janvier

Objectifs de la journée : produire un ouvrage en HTML, Pdf (LaTeX) et Epub, à partir d'une édition Markdown.

5. Initialisation de l'ouvrage ([parcours/05_livre](./parcours/05_livre.md))
6. Production du HTML ([parcours/06_livreHTML](./parcours/06_livreHTML.md))
7. Production du PDF ([parcours/07_livreLatex](./parcours/07_livreLatex.md))
8. Production de l'Epub ([parcours/08_livreEpub](./parcours/08_livreEpub.md))

## Pour aller plus loin

- créer une chaine en automatisant une série d'actions ou de commandes en utilisant les scripts bash.
  - Par exemple, pour produire d'un coup un epub et un pdf à partir d'une source markdown, voir le script [ressources/build.sh](./ressources/build.sh).
  - Voir aussi [cette petite documentation](https://doc.ubuntu-fr.org/tutoriel/script_shell) pour créer des scripts.
- utiliser des filtres pour créer des traitements particuliers selon vos sorties (par exemple «supprimer tous les titres de niveau 3 pour les exports Pdf»).
  - Voir la documentation [Filtres](http://pandoc.org/filters.html) et [Filtres Lua](http://pandoc.org/lua-filters.html).
- utiliser des [préprocesseurs](https://github.com/jgm/pandoc/wiki/Pandoc-Extras#preprocessors) pour complexifier le markdown source.
- consulter le [wiki Pandoc](https://github.com/jgm/pandoc/wiki) qui référence de multiples ressources.
---
[Démarrer](./parcours/00_introduction.md)
